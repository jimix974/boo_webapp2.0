<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {		
        $this->db->select('u.id, u.email, u.name, u.mobile, Roles.role');
        $this->db->from('Users as u');
        $this->db->join('Roles as Roles', 'Roles.id = u.roleId','left');
        if(!empty($searchText)) { $this->db->or_like('u.email', $searchText); $this->db->or_like('u.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
        $this->db->where('u.isDeleted', 0);
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {	 
        $this->db->select('u.id as userId, u.email, u.name, u.mobile, Roles.role');
        $this->db->from('Users as u');
        $this->db->join('Roles as Roles', 'Roles.id = u.roleId','left');
        if(!empty($searchText)) { $this->db->or_like('u.email', $searchText); $this->db->or_like('u.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
        $this->db->where('u.isDeleted', 0);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {   
        $this->db->select('id as roleId, role');
        $this->db->from('Roles');
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    { 
        $this->db->trans_start();
        $this->db->insert('Users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    { 
        $this->db->select('id as userId, name, email, mobile, roleId');
        $this->db->from('Users');
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId !=', 1);
        $this->db->where('id', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {   
        $this->db->where('userId', $userId);
        $this->db->update('Users', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->update('Users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId');
        $this->db->where('userId', $userId);
        $this->db->where('password', $oldPassword);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('Users');
        
        return $query->result();
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('Users', $userInfo);
        
        return $this->db->affected_rows();
    }
}

  