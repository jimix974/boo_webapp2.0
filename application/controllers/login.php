<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');        
        $this->load->model('router_model');
        $this->load->helper('security');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->isLoggedIn();
    }
    
    /**
     * This function used to check the user is logged in or not
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            $this->load->view('login');
        }
        else
        {
            redirect('/dashboard');
        }
    }
    
    
    /**
     * This function used to logged in user
     */
    public function loginMe()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean|trim');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            
            $ecryptPassword = md5($password);
            
            $result = $this->login_model->loginMe($email, $ecryptPassword);
            
            if(count($result) > 0)
            {
                foreach ($result as $res)
                {
                    $sessionArray = array('userId'=>$res->userId,                    
                                            'role'=>$res->roleId,
                                            'roleText'=>$res->role,
                                            'name'=>$res->name,
                    						'nurseryId'=>$res->Nursery_id,
                    						'nurseryGroupId'=>$res->Nursery_groupid,
                                            'isLoggedIn' => TRUE
                                    );
                    $this->session->set_userdata($sessionArray);
                    
                    /* load the target DB 
                    //get the nurseryId or nurseryGroupId
                    $nId='';
                    if ($res->Nursery_groupid===null || $res->Nursery_groupid==-1 )
                    {                
                    	$nId=$res->Nursery_id;
                    }
                    else 
                    {
						$nId=$res->Nursery_groupid;  
                    }
                                        
                    //get the serverUrl from nurseryRouter 
                    $serveurUrlRes = $this->router_model->getDBUrlForId($nId);
                    if(count($serveurUrlRes) == 1)
                    {
                    	foreach ($serveurUrlRes as $ures)
                    	{
                    		$serveurUrl=$ures->serverBdd;
                    	}	 
                    }
                    //echo $serveurUrl;
                    $this->router_model->loadTargetDb($serveurUrl);
                    
					*/
					
                    
                    redirect('/dashboard');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                redirect('/login');
            }
        }
    }
}

?>