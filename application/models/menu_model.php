<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model
{
	/**
	 * for super admin
	 */
	private function queryMenuListingAll($searchText = '',  $page = '', $segment = '')
	{
        $this->db->select('m.id, m.date, m.starter, m.main, m.dessert, m.snack, n.name as nurseryName');
        $this->db->from('Menu as m');
        $this->db->join('Nursery as n', 'n.id = m.Nursery_id');
		$this->db->where('m.isDeleted', 0);
		$this->db->order_by('m.date', 'DESC');
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		// manage pagination
		if(!empty($page) && !empty($segment)) {
        	$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for group admin
	 */
	private function queryMenuListingGroup($searchText = '', $groupId = '', $page = '', $segment = '')
	{
        $this->db->select('m.id, m.date, m.starter, m.main, m.dessert, m.snack, n.name as nurseryName');
        $this->db->from('Menu as m');
        $this->db->join('Nursery as n', 'n.id = m.Nursery_id');
		$this->db->join('NurseriesGroup as ng', 'ng.Nursery_id = n.id');
		$this->db->where('m.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		if(!empty($groupId)) {
			$this->db->where('ng.NurseryGroup_id', $groupId);
		}
		
		$this->db->order_by('m.date', 'DESC');
		
		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for nursery admin
	 */
	private function queryMenuListingNursery($searchText = '', $nurseryId = '', $page = '', $segment = '')
	{
        $this->db->select('m.id, m.date, m.starter, m.main, m.dessert, m.snack, n.name as nurseryName');
        $this->db->from('Menu as m');
        $this->db->join('Nursery as n', 'n.id = m.Nursery_id');
		$this->db->where('m.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		if(!empty($nurseryId)) {
			$this->db->where('n.id', $nurseryId);
		}
		
		$this->db->order_by('m.date', 'DESC');
		
		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	

	/**
	 * This function is used to get the menu listing count for super admin
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function menuListingCountAll($searchText = '')
	{
		$query = $this->queryMenuListingAll($searchText);
		return count($query->result());
	}
	
	/**
	 * This function is used to get the menu listing count for super admin
	 * @param string $searchText : This is optional search text
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function menuListingAll($searchText = '', $page, $segment)
	{
		$query = $this->queryMenuListingAll($searchText, $page, $segment);
		return $query->result();
	}
	
	/**
	 * This function is used to get the menu listing count for a nursery group
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function menuListingGroupCount($searchText = '', $groupId = '')
	{
		$query = $this->queryMenuListingGroup($searchText, $groupId);
		 
		return count($query->result());
	}
	
	/**
	 * This function is used to get the menu listing count for a nursery group
	 * @param string $searchText : This is optional search text
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function menuListingGroup($searchText = '', $page, $segment, $groupId = '')
	{
		$query = $this->queryMenuListingGroup($searchText, $groupId, $page, $segment);
		return $query->result();
	}
	
	/**
	 * This function is used to get the menu listing count for a nursery
	 * @param string $searchText : This is optional search text
	 * @return number $count : This is row count
	 */
	function menuListingNurseryCount($searchText = '', $nurseryId = '')
	{
		$query = $this->queryMenuListingNursery($searchText, $nurseryId);
	
		return count($query->result());
	}
	
	/**
	 * This function is used to get the menu listing count for a nursery
	 * @param string $searchText : This is optional search text
	 * @param number $page : This is pagination offset
	 * @param number $segment : This is pagination limit
	 * @return array $result : This is result
	 */
	function menuListingNursery($searchText = '', $page, $segment, $nurseryId = '')
	{
		$query = $this->queryMenuListingNursery($searchText, $nurseryId, $page, $segment);
		 
		return $query->result();
	}
	 
	/**
	 * This function is used to add new menu to system
	 * @return number $insert_id : This is last inserted id
	 */
	function addNewMenu($menuInfo)
	{
		$this->db->trans_start();
		$this->db->insert('Menu', $menuInfo);
	
		$insert_id = $this->db->insert_id();
	
		$this->db->trans_complete();
	
		return $insert_id;
	}
   
}

  