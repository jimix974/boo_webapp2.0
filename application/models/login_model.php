<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $ecryptPassword)
    {
        $this->db->select('u.id as userId, u.name, u.roleId, Roles.role, u.Nursery_id, u.Nursery_groupid,');
        $this->db->from('Users as u');
        $this->db->join('Roles as Roles','Roles.Id = u.roleId');
        $this->db->where('u.email', $email);
        $this->db->where('u.password', $ecryptPassword);
        $this->db->where('u.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
}

?>