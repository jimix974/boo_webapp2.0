<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Section extends CI_Controller

{
    protected $role = ''; 
    protected $userId = '';
    protected $name = '';
    protected $roleText = '';
    protected $global = array();

    public function __construct()
    {
        parent::__construct(); 
        $this->isLoggedIn();
        $this->load->helper('security'); 
    	$this->load->model('section_model');    
    }
    
    public function sectionListing()
    {    		    	 
    		$searchText = $this->input->post('searchText');
    		$data['searchText'] = $searchText;
    	
    		$this->load->library('pagination');
    		
    		switch ($this->role) {
    			case ROLE_ADMIN:
	    			$count = $this->section_model->sectionListingCountAll($searchText);
    				break;
    			case ROLE_MANAGER:
    				$count = $this->section_model->sectionListingGroupCount($searchText, $this->session->nurseryGroupId);
    				break;
    			default:
    				$count = $this->section_model->sectionListingNurseryCount($searchText, $this->session->nurseryId);
    				break;
    		}
    		
    		$config['base_url'] = base_url().'index.php/section/sectionListing/';
    		$config['total_rows'] = $count;
    		$config['uri_segment'] = 2;
    		$config['per_page'] = 5;
    		$config['num_links'] = 5;
    		$config['full_tag_open'] = '<nav><ul class="pagination">';
    		$config['full_tag_close'] = '</ul></nav>';
    		$config['first_tag_open'] = '<li class="arrow">';
    		$config['first_link'] =  'First';
    		$config['first_tag_close'] = '</li>';
    		$config['prev_link'] = 'Previous';
    		$config['prev_tag_open'] = '<li class="arrow">';
    		$config['prev_tag_close'] = '</li>';
    		$config['next_link'] = 'Next';
    		$config['next_tag_open'] = '<li class="arrow">';
    		$config['next_tag_close'] = '</li>';
    		$config['cur_tag_open'] = '<li class="active"><a href="#">';
    		$config['cur_tag_close'] = '</a></li>';
    		$config['num_tag_open'] = '<li>';
    		$config['num_tag_close'] = '</li>';
    		$config['last_tag_open'] = '<li class="arrow">';
    		$config['last_link'] = 'Last';
    		$config['last_tag_close'] = '</li>';


    		$this->pagination->initialize($config);
    		$page = $config['per_page'];
    		$segment = $this->uri->segment(2);
    	
    		switch ($this->role) {
    			case ROLE_ADMIN:
    				$data['sectionRecords'] = $this->section_model->sectionListingAll($searchText, $page, $segment);
    				$data['displayNursery'] = 'true';
    				break;
    			case ROLE_MANAGER:
    				$data['sectionRecords'] = $this->section_model->sectionListingGroup($searchText, $page, $segment, $this->session->nurseryGroupId);
    				$data['displayNursery'] = 'true';
    				break;
    			default:
    				$data['sectionRecords'] = $this->section_model->sectionListingNursery($searchText, $page, $segment, $this->session->nurseryId);
					break;
    		}

    		$this->global['pageTitle'] = 'BoO : Liste section';
    		$this->load->view('includes/header', $this->global);
    		$this->load->view('sections', $data);
    		$this->load->view('includes/footer');

    }
    
    function isLoggedIn()
    {
    	$isLoggedIn = $this->session->userdata('isLoggedIn');
    
    	if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
    	{
    		redirect('/login');
    	}
    	else
    	{
    		$this->role = $this->session->userdata('role');
    		$this->userId = $this->session->userdata('userId');
    		$this->name = $this->session->userdata('name');
    		$this->roleText = $this->session->userdata('roleText');
    		$this->userGroupId = $this->session->userdata('groupId');
    		$this->userNurseryId = $this->session->userdata('nurseryId');
    
    		$this->global['name'] = $this->name;
    		$this->global['role'] = $this->role;
    		$this->global['role_text'] = $this->roleText;
    	}
    }

    function addNew()
    {
    	//Everyone can add a section carefull : group have to select nursery before
    	$this->load->model('nursery_model');
    	 
    	switch ($this->role) {
    		case ROLE_ADMIN:
    			$data['nurseries'] = $this->nursery_model->nurseryListingAll('', '', '');
    			$data['displayNursery'] = 'true';
    			break;
    		case ROLE_MANAGER:
    			$data['nurseries'] = $this->nursery_model->nurseryListingGroup('', '', '', $this->session->nurseryGroupId);
    			$data['displayNursery'] = 'true';
    			break;
    		default:
    			$data['nurseries'] = '';
    			break;
    	}
        $this->global['pageTitle'] = 'BoO : Nouvelle section';
        $this->load->view('includes/header', $this->global);
        $this->load->view('sectionNew', $data);
        $this->load->view('includes/footer');
        
    }

    

    /**
     * This function is used to add new section to the system
     */
    function addNewSection()
    {
    		$this->load->library('form_validation');
    
    		//validate form
    		$this->form_validation->set_rules('name','Nom de la section','trim|required|max_length[128]|xss_clean');
    		
    		if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    		{
    			$this->form_validation->set_rules('nursery','Crèche','trim|required|numeric');
    		}
    		
    		if($this->form_validation->run() == FALSE)
    		{
    			$this->sectionNew();
    		}
    		else
    		{
    			$name = ucwords(strtolower($this->input->post('name')));

    			if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    			{
    				$nurseryId = $this->input->post('nursery');
    			}
    			else 
    			{
    				$nurseryId = $this->userNurseryId;
    			}
    			
    			$this->load->model('section_model');
    			
    			$sectionInfo = array('name'=>$name, 'Nursery_id'=>$nurseryId, 
    					'createdBy'=>$this->userId, 'createdDtm'=>date('Y-m-d H:i:s'));
    
    			$result = $this->section_model->addNewSection($sectionInfo);
    
    			if($result > 0)
    			{
    				$this->session->set_flashdata('success', 'Nouvells section créée');
    			}
    			else
    			{
    				$this->session->set_flashdata('error', 'Echec de la création de la section');
    			}
    
    			redirect('section/sectionListing');
    		}
    }
    
    
    /**
     * This function is used to edit and update a section information
     * @param number $sectionId : Optional : This is section id
     */
    function editSection($sectionId = NULL)
    {
    	$this->load->model('nursery_model');
    	 
    	if($sectionId == null)
    	{
   			redirect('sectionListing');
   		}
   		
   		switch ($this->role) {
   			case ROLE_ADMIN:
   				$data['nurseries'] = $this->nursery_model->nurseryListingAll('', '', '');
   				$data['displayNursery'] = 'true';
   				break;
   			case ROLE_MANAGER:
   				$data['nurseries'] = $this->nursery_model->nurseryListingGroup('', '', '', $this->session->nurseryGroupId);
   				$data['displayNursery'] = 'true';
   				break;
   			default:
   				$data['nurseries'] = '';
   				break;
   		}
   		
    	$data['sectionInfo'] = $this->section_model->getSectionInfo($sectionId);
    
    	$this->global['pageTitle'] = 'CodeInsect : Edit Section';
    	$this->load->view('includes/header', $this->global);
    	$this->load->view('editSection', $data);
    	$this->load->view('includes/footer');
    	
    }
    
    /**
     * This function is used to update the section information
     */
    function updateSection()
    {

    		$this->load->library('form_validation');
    
    		$sectionId = $this->input->post('sectionId');
    
        	$this->form_validation->set_rules('name','Nom de la section','trim|required|max_length[128]|xss_clean');
    		
    		if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    		{
    			$this->form_validation->set_rules('nursery','Crèche','trim|required|numeric');
    		}
    
    		if($this->form_validation->run() == FALSE)
    		{
    			$this->editSection($sectionId);
    		}
    		else
    		{
    			$name = ucwords(strtolower($this->input->post('name')));
    			
    			if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    			{
    				$nurseryId = $this->input->post('nursery');
    			}
    			else
    			{
    				$nurseryId = $this->userNurseryId;
    			}
    			 
    			$sectionInfo = array('name'=>$name, 'Nursery_id'=>$nurseryId,
    					'updatedBy'=>$this->userId, 'updatedDtm'=>date('Y-m-d H:i:s'));
    			
    			$result = $this->section_model->updateSection($sectionInfo, $sectionId);
    
    			if($result == true)
    			{
    				$this->session->set_flashdata('success', 'Section updated successfully');
    			}
    			else
    			{
    				$this->session->set_flashdata('error', 'Section updation failed');
    			}
    
    			redirect('sectionListing');
    		
    	}
    }
    
    
    /**
     * This function is used to delete the section using sectionId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSection()
    {
    	if($this->isAdmin() == TRUE)
    	{
    		echo(json_encode(array('status'=>'access')));
    	}
    	else
    	{
    		$sectionId = $this->input->post('sectionId');
    		$sectionInfo = array('isDeleted'=>1,'updatedBy'=>$this->userId, 'updatedDtm'=>date('Y-m-d H:i:sa'));
    
    		$result = $this->section_model->deleteSection($sectionId, $sectionInfo);
    
    		if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
    		else { echo(json_encode(array('status'=>FALSE))); }
    	}
    }
    
}