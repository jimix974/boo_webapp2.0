<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class creche_model extends CI_Model
{
	/**
	 * for super admin
	 */
	private function queryNurseryListingAll($searchText = '',  $page = '', $segment = '')
	{
		$this->db->select('n.id, n.name, n.address, n.email, n.tel, n.idUnique, n.zipCode');
		$this->db->from('Nursery as n');
		$this->db->where('n.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		// manage pagination
		if(!empty($page) && !empty($segment)) {
        	$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for group admin
	 */
	private function queryNurseryListingGroup($searchText = '', $groupId = '', $page = '', $segment = '')
	{
		$this->db->select('n.id, n.name, n.address, n.email, n.tel, n.idUnique, n.zipCode');
		$this->db->from('Nursery as n');
		$this->db->join('NurseriesGroup as ng', 'ng.Nursery_id = n.id');
		$this->db->where('n.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
		//$this->db->where('BaseTbl.isDeleted', 0);
	
		if(!empty($groupId)) {
			$this->db->where('ng.NurseryGroup_id', $groupId);
		}

		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for nursery admin
	 */
	private function queryNursery($searchText = '', $nurseryId = '', $page = '', $segment = '')
	{
		$this->db->select('n.id, n.name, n.address, n.email, n.tel, n.idUnique, n.zipCode');
		$this->db->from('Nursery as n');
		$this->db->where('n.isDeleted', 0);
	
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		if(!empty($nurseryId)) {
			$this->db->where('n.id', $nurseryId);
		}
	
		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
    /**
     * This function is used to get the nursery listing count for super admin
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function crecheListingCountAll($searchText = '')
    {		     	
    	$query = $this->queryNurseryListingAll($searchText);
        return count($query->result());
    }
    
    /**
     * This function is used to get the nusery listing count for super admin
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function crecheListingAll($searchText = '', $page = '', $segment = '')
    {    	 
    	$query = $this->queryNurseryListingAll($searchText, $page, $segment);
        return $query->result();
    }
    
    /**
     * This function is used to get the nursery listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function crecheListingGroupCount($searchText = '', $groupId = '')
    {
    	$query = $this->queryNurseryListingGroup($searchText, $groupId);
    	
    	return count($query->result());
    }
    
    /**
     * This function is used to get the nrsery listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function crecheListingGroup($searchText = '', $page = '', $segment = '', $groupId = '')
    {
    	$query = $this->queryNurseryListingGroup($searchText, $groupId, $page, $segment);
    	return $query->result();
    }

    
    /**
     * This function is used to get one nursery infos
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function crecheListingNursery($searchText = '', $page = '', $segment = '', $nurseryId = '')
    {
    	$query = $this->queryNursery($searchText, $nurseryId, $page, $segment);
    	
    	return $query->result();
    }
   
}

  