<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Router_model extends CI_Model
{
    
    /**
     * This function used to get the db url 
     * @param string $id : id of the nurserygroupid or nurseryid
     */
    function getDBUrlForId( $id)
    {
        $this->db->select('serverBdd');
        $this->db->from('NurseryRouter');
        $this->db->where('nurseryId', $id);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    /**
     * This function used to update the targetdb url
     * @param string $url : url of the targetdb 
     */
    function loadTargetDb( $url)
    {    
		$config['hostname'] = $url;
		$config['username'] = 'kreoBoo';
		$config['password'] = 'kreoBoo97';
		$config['database'] = 'boo';
		$config['dbdriver'] = 'mysqli';
		$config['dbprefix'] = '';
		$config['pconnect'] = FALSE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = '';
		$config['char_set'] = 'utf8';
		$config['dbcollat'] = 'utf8_general_ci';
		$TARGETDB = $this->load->database($config, TRUE);		
			
		$dbArray = array('targetDB'=>$TARGETDB);
		$this->session->set_userdata($dbArray);
    	/*
    	echo $this->db->database;
    	
		$this->db->db_select("targetDB");

		echo $this->db->database;
		$this->db->update_hostname($url);
    	$this->db->reconnect();    	
    	
    	$this->load->database('targetDB'); 
    	echo $this->db->database;*/
    }
}

?>