<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Menu extends CI_Controller

{
    protected $role = ''; 
    protected $userId = '';
    protected $name = '';
    protected $roleText = '';
    protected $global = array();

    public function __construct()
    {
        parent::__construct(); 
        $this->isLoggedIn();
        $this->load->helper('security'); 
    	$this->load->model('menu_model');    
    }
    
    public function menuListing()
    {    		
    		$searchText = $this->input->post('searchText');
    		$data['searchText'] = $searchText;
    	
    		$this->load->library('pagination');

    		switch ($this->role) {
    			case ROLE_ADMIN:
    				$count = $this->menu_model->menuListingCountAll($searchText);
    				break;
    			case ROLE_MANAGER:
    				$count = $this->menu_model->menuListingGroupCount($searchText, $this->session->nurseryGroupId);
    				break;
    			default:
    				$count = $this->menu_model->menuListingNurseryCount($searchText, $this->session->nurseryId);
    				break;
    		}
    		  	
    		$config['base_url'] = base_url().'index.php/menu/menuListing/';
    		$config['total_rows'] = $count;
    		$config['uri_segment'] = 2;
    		$config['per_page'] = 5;
    		$config['num_links'] = 5;
    		$config['full_tag_open'] = '<nav><ul class="pagination">';
    		$config['full_tag_close'] = '</ul></nav>';
    		$config['first_tag_open'] = '<li class="arrow">';
    		$config['first_link'] =  'First';
    		$config['first_tag_close'] = '</li>';
    		$config['prev_link'] = 'Previous';
    		$config['prev_tag_open'] = '<li class="arrow">';
    		$config['prev_tag_close'] = '</li>';
    		$config['next_link'] = 'Next';
    		$config['next_tag_open'] = '<li class="arrow">';
    		$config['next_tag_close'] = '</li>';
    		$config['cur_tag_open'] = '<li class="active"><a href="#">';
    		$config['cur_tag_close'] = '</a></li>';
    		$config['num_tag_open'] = '<li>';
    		$config['num_tag_close'] = '</li>';
    		$config['last_tag_open'] = '<li class="arrow">';
    		$config['last_link'] = 'Last';
    		$config['last_tag_close'] = '</li>';

    		$this->pagination->initialize($config);
    		$page = $config['per_page'];
    		$segment = $this->uri->segment(2);
    	
    		switch ($this->role) {
    			case ROLE_ADMIN:
    				$data['menuRecords'] = $this->menu_model->menuListingAll($searchText, $page, $segment);
    				$data['displayNursery'] = 'true';
    				break;
    			case ROLE_MANAGER:
    				$data['menuRecords'] = $this->menu_model->menuListingGroup($searchText, $page, $segment, $this->session->nurseryGroupId);
    				$data['displayNursery'] = 'true';
    				break;
    			default:
    				$data['menuRecords'] = $this->menu_model->menuListingNursery($searchText, $page, $segment, $this->session->nurseryId);
    				break;
    		}

    		$this->global['pageTitle'] = 'CodeInsect : child Listing';
    		$this->load->view('includes/header', $this->global);
    		$this->load->view('menus', $data);
    		$this->load->view('includes/footer');

    }

    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        
        if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('/login');
        }
        else
        {
            $this->role = $this->session->userdata('role');
            $this->userId = $this->session->userdata('userId');
            $this->name = $this->session->userdata('name');
            $this->roleText = $this->session->userdata('roleText');
    		$this->userGroupId = $this->session->userdata('groupId');
    		$this->userNurseryId = $this->session->userdata('nurseryId');
            
            $this->global['name'] = $this->name;
            $this->global['role'] = $this->role;
            $this->global['role_text'] = $this->roleText;
        }
    }

    function addNew()
    {
    	//Everyone can add a menu carefull : group have to select nursery before
    	$this->load->model('nursery_model');
    	
    	switch ($this->role) {
    		case ROLE_ADMIN:
    			$data['nurseries'] = $this->nursery_model->nurseryListingAll('', '', '');
    			$data['displayNursery'] = 'true';
    			break;
    		case ROLE_MANAGER:
    			$data['nurseries'] = $this->nursery_model->nurseryListingGroup('', '', '', $this->session->nurseryGroupId);
    			$data['displayNursery'] = 'true';
    			break;
    		default:
    			$data['nurseries'] = '';
    			break;
    	}
        
        $this->global['pageTitle'] = 'BoO : Nouveau menu';
        $this->load->view('includes/header', $this->global);
        $this->load->view('menuNew', $data);
        $this->load->view('includes/footer');
        
    }

    
    /**
     * This function is used to add new menu to the system
     */
    function addNewMenu()
    {
    	$this->load->library('form_validation');
    
    	//validate form
    	$this->form_validation->set_rules('date','Date','trim|max_length[128]|xss_clean');
    	$this->form_validation->set_rules('starter','Entrée','trim|max_length[128]|xss_clean');
    	$this->form_validation->set_rules('main','Plat','trim|max_length[128]|xss_clean');
    	$this->form_validation->set_rules('dessert','Dessert','trim|max_length[128]|xss_clean');
    	$this->form_validation->set_rules('snack','Goûter','trim|max_length[128]|xss_clean');
    	 
    	if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    	{
    		$this->form_validation->set_rules('nursery','Crèche','trim|required|numeric');
    	}
    
    	if($this->form_validation->run() == FALSE)
    	{
    		$this->menuNew();
    	}
    	else
    	{   
    		$date = $this->input->post('date');
    		$starter = $this->input->post('starter');
    		$main = $this->input->post('main');
    		$dessert = $this->input->post('dessert');
    		$snack = $this->input->post('snack');
    		
    		if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    		{
    			$nurseryId = $this->input->post('nursery');
    		}
    		else
    		{
    			$nurseryId = $this->userNurseryId;
    		}
    		 
    		$this->load->model('menu_model');
    		 
    		//weeknumber

    		$d = new DateTime($date);
    		$weekNum = $d->format("W");
    		$dformatted = date_format($d, 'Y-m-d');
    		
    		
    		$menuInfo = array('date'=>$dformatted, 'weekNumber'=>$weekNum, 'starter'=>$starter, 'main'=>$main, 'dessert'=>$dessert, 'snack'=>$snack, 'Nursery_id'=>$nurseryId,
    				'createdBy'=>$this->userId, 'createdDtm'=>date('Y-m-d H:i:s'));
    
    		$result = $this->menu_model->addNewMenu($menuInfo);
    
    		if($result > 0)
    		{
    			$this->session->set_flashdata('success', 'Nouveau menu créé');
    		}
    		else
    		{
    			$this->session->set_flashdata('error', 'Echec de la création du menu');
    		}
    
    		redirect('menu/menuListing');
    	}
    }
}