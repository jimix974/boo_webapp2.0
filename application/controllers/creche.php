<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Creche extends CI_Controller

{
    protected $role = ''; 
    protected $userId = '';
    protected $name = '';
    protected $roleText = '';
    protected $global = array();

    public function __construct()
    {
        parent::__construct(); 
        $this->isLoggedIn();
        $this->load->helper('security'); 
    	$this->load->model('creche_model');    
    }
    
    public function crecheListing()
    {    		    	 
    		$searchText = $this->input->post('searchText');
    		$data['searchText'] = $searchText;
    	
    		$this->load->library('pagination');
    		
    		switch ($this->role) {
    			case ROLE_ADMIN:
	    			$count = $this->creche_model->crecheListingCountAll($searchText);
    				break;
    			case ROLE_MANAGER:
    				$count = $this->creche_model->crecheListingGroupCount($searchText, $this->session->nurseryGroupId);
    				break;
    			default:
    				$count = $this->creche_model->crecheListingNurseryCount($searchText, $this->session->nurseryId);
    				break;
    		}
    		
    		$config['base_url'] = base_url().'index.php/creche/crecheListing/';
    		$config['total_rows'] = $count;
    		$config['uri_segment'] = 2;
    		$config['per_page'] = 5;
    		$config['num_links'] = 5;
    		$config['full_tag_open'] = '<nav><ul class="pagination">';
    		$config['full_tag_close'] = '</ul></nav>';
    		$config['first_tag_open'] = '<li class="arrow">';
    		$config['first_link'] =  'First';
    		$config['first_tag_close'] = '</li>';
    		$config['prev_link'] = 'Previous';
    		$config['prev_tag_open'] = '<li class="arrow">';
    		$config['prev_tag_close'] = '</li>';
    		$config['next_link'] = 'Next';
    		$config['next_tag_open'] = '<li class="arrow">';
    		$config['next_tag_close'] = '</li>';
    		$config['cur_tag_open'] = '<li class="active"><a href="#">';
    		$config['cur_tag_close'] = '</a></li>';
    		$config['num_tag_open'] = '<li>';
    		$config['num_tag_close'] = '</li>';
    		$config['last_tag_open'] = '<li class="arrow">';
    		$config['last_link'] = 'Last';
    		$config['last_tag_close'] = '</li>';

    		$this->pagination->initialize($config);
    		$page = $config['per_page'];
    		$segment = $this->uri->segment(2);
    	
    		switch ($this->role) {
    			case ROLE_ADMIN:
    				$data['crecheRecords'] = $this->creche_model->crecheListingAll($searchText, $page, $segment);
    				$data['displayNursery'] = 'true';
    				break;
    			case ROLE_MANAGER:
    				$data['crecheRecords'] = $this->creche_model->crecheListingGroup($searchText, $page, $segment, $this->session->nurseryGroupId);
    				$data['displayNursery'] = 'true';
    				break;
    			default:
    				$data['crecheRecords'] = $this->creche_model->crecheListingNursery($searchText, $page, $segment, $this->session->nurseryId);
					break;
    		}

    		$this->global['pageTitle'] = 'BoO : Liste crèche';
    		$this->load->view('includes/header', $this->global);
    		$this->load->view('creches', $data);
    		$this->load->view('includes/footer');

    }
    
    function isLoggedIn()
    {
    	$isLoggedIn = $this->session->userdata('isLoggedIn');
    
    	if(!isset($isLoggedIn) || $isLoggedIn != TRUE)
    	{
    		redirect('/login');
    	}
    	else
    	{
    		$this->role = $this->session->userdata('role');
    		$this->userId = $this->session->userdata('userId');
    		$this->name = $this->session->userdata('name');
    		$this->roleText = $this->session->userdata('roleText');
    		$this->userGroupId = $this->session->userdata('groupId');
    		$this->userNurseryId = $this->session->userdata('nurseryId');
    
    		$this->global['name'] = $this->name;
    		$this->global['role'] = $this->role;
    		$this->global['role_text'] = $this->roleText;
    	}
    }

    function addNew()
    {
    	//Everyone can add a section carefull : group have to select nursery before
    	$this->load->model('nursery_model');
    	 
    	switch ($this->role) {
    		case ROLE_ADMIN:
    			$data['nurseries'] = $this->nursery_model->nurseryListingAll('', '', '');
    			$data['displayNursery'] = 'true';
    			break;
    		case ROLE_MANAGER:
    			$data['nurseries'] = $this->nursery_model->nurseryListingGroup('', '', '', $this->session->nurseryGroupId);
    			$data['displayNursery'] = 'true';
    			break;
    		default:
    			$data['nurseries'] = '';
    			break;
    	}
        $this->global['pageTitle'] = 'BoO : Nouvelle crèche';
        $this->load->view('includes/header', $this->global);
        $this->load->view('crecheNew', $data);
        $this->load->view('includes/footer');
        
    }

    

    /**
     * This function is used to add new section to the system
     */
    function addNewCreche()
    {
    		$this->load->library('form_validation');
    
    		//validate form
    		$this->form_validation->set_rules('name','Nom de la crèche','trim|required|max_length[128]|xss_clean');
    		
    		if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    		{
    			$this->form_validation->set_rules('nursery','Crèche','trim|required|numeric');
    		}
    		
    		if($this->form_validation->run() == FALSE)
    		{
    			$this->crecheNew();
    		}
    		else
    		{
    			$name = ucwords(strtolower($this->input->post('name')));

    			if ($this->role == ROLE_ADMIN || $this->role == ROLE_MANAGER)
    			{
    				$nurseryId = $this->input->post('nursery');
    			}
    			else 
    			{
    				$nurseryId = $this->userNurseryId;
    			}
    			
    			$this->load->model('creche_model');
    			
    			$crecheInfo = array('name'=>$name, 'Nursery_id'=>$nurseryId, 
    					'createdBy'=>$this->userId, 'createdDtm'=>date('Y-m-d H:i:s'));
    
    			$result = $this->creche_model->addNewCreche($crecheInfo);
    
    			if($result > 0)
    			{
    				$this->session->set_flashdata('success', 'Nouvel utilisateur créé');
    			}
    			else
    			{
    				$this->session->set_flashdata('error', "Echec de la création de l'utilisateur");
    			}
    
    			redirect('creche/crecheListing');
    		}
    }
    
}