<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Section_model extends CI_Model
{
	/**
	 * for super admin
	 */
	private function querySectionListingAll($searchText = '',  $page = '', $segment = '')
	{
		$this->db->select('s.id, s.name, t.idUnique as tabletId, n.name as nurseryName');
		$this->db->from('Section as s');
		$this->db->join('Nursery as n', 'n.id = s.Nursery_id');
		// left join with Tablet because we also want section which don't have Tablet yet
		$this->db->join('Tablet as t', 't.id = s.Tablet_id', 'left');
		$this->db->where('s.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		// manage pagination
		if(!empty($page) && !empty($segment)) {
        	$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for group admin
	 */
	private function querySectionListingGroup($searchText = '', $groupId = '', $page = '', $segment = '')
	{
		$this->db->select('s.id, s.name, t.idUnique as tabletId, n.name as nurseryName');
		$this->db->from('Section as s');				
		$this->db->join('Nursery as n', 'n.id = s.Nursery_id');
		$this->db->join('NurseriesGroup as ng', 'ng.Nursery_id = n.id');
		// left join with Tablet because we also want section which don't have Tablet yet
		$this->db->join('Tablet as t', 't.id = s.Tablet_id', 'left');
		$this->db->where('s.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		if(!empty($groupId)) {
			$this->db->where('ng.NurseryGroup_id', $groupId);
		}

		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	/**
	 * for nursery admin
	 */
	private function querySectionListingNursery($searchText = '', $nurseryId = '', $page = '', $segment = '')
	{
		$this->db->select('s.id, s.name, t.idUnique as tabletId, n.name as nurseryName');
		$this->db->from('Section as s');
		$this->db->join('Nursery as n', 'n.id = s.Nursery_id');
		// left join with Tablet because we also want section which don't have Tablet yet
		$this->db->join('Tablet as t', 't.id = s.Tablet_id', 'left');
		$this->db->where('s.isDeleted', 0);
		
		//TODO
		//if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
	
		if(!empty($nurseryId)) {
			$this->db->where('s.Nursery_id', $nurseryId);
		}
	
		// manage pagination
		if(!empty($page) && !empty($segment)) {
			$this->db->limit($page, $segment);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
    /**
     * This function is used to get the section listing count for super admin
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function sectionListingCountAll($searchText = '')
    {		     	
    	$query = $this->querySectionListingAll($searchText);
        return count($query->result());
    }
    
    /**
     * This function is used to get the section listing count for super admin
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function sectionListingAll($searchText = '', $page, $segment)
    {    	 
    	$query = $this->querySectionListingAll($searchText, $page, $segment);
        return $query->result();
    }
    
    /**
     * This function is used to get the section listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function sectionListingGroupCount($searchText = '', $groupId = '')
    {
    	$query = $this->querySectionListingGroup($searchText, $groupId);
    	
    	return count($query->result());
    }
    
    /**
     * This function is used to get the section listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function sectionListingGroup($searchText = '', $page, $segment, $groupId = '')
    {
    	$query = $this->querySectionListingGroup($searchText, $groupId, $page, $segment);
    	return $query->result();
    }
    
    /**
     * This function is used to get the section listing count for a nursery 
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function sectionListingNurseryCount($searchText = '', $nurseryId = '')
    {
    	$query = $this->querySectionListingNursery($searchText, $nurseryId);
    	 
    	return count($query->result());
    }
    
    /**
     * This function is used to get the section listing count for a nursery 
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function sectionListingNursery($searchText = '', $page, $segment, $nurseryId = '')
    {
    	$query = $this->querySectionListingNursery($searchText, $nurseryId, $page, $segment);
    	
    	return $query->result();
    }
   
    /**
     * This function is used to add new section to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewSection($sectionInfo)
    {
    	$this->db->trans_start();
    	$this->db->insert('Section', $sectionInfo);
    
    	$insert_id = $this->db->insert_id();
    
    	$this->db->trans_complete();
    
    	return $insert_id;
    }
    
    
    /**
     * This function used to get section information by id
     * @param number $sectionId : This is section id
     * @return array $result : This is section information
     */
    function getSectionInfo($sectionId)
    {
		$this->db->select('s.id, s.name, t.idUnique as tabletId, n.name as nurseryName, n.id as nurseryId');
		$this->db->from('Section as s');
		$this->db->join('Nursery as n', 'n.id = s.Nursery_id');
		$this->db->join('Tablet as t', 't.id = s.Tablet_id', 'left');
		$this->db->where('s.isDeleted', 0);
    	$this->db->where('s.id', $sectionId);
    	$query = $this->db->get();
    
    	return $query->result();
    }
    
    
    /**
     * This function is used to update the section information
     * @param array $sectionInfo : This is section updated information
     * @param number $sectionId : This is section id
     */
    function updateSection($sectionInfo, $sectionId)
    {
    	$this->db->where('id', $sectionId);
    	$this->db->update('Section', $sectionInfo);
    
    	return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the section information
     * @param number $sectionId : This is section id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteSection($sectionId, $sectionInfo)
    {
    	$this->db->where('id', $sectionId);
    	$this->db->update('Section', $sectionInfo);
    
    	return $this->db->affected_rows();
    }
}

  