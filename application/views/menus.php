<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestion des Menus
        <small>Ajouter, Modifier, Supprimer</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/menu/importFile">Importer un fichier</a>
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/menu/addNew">Ajouter un menu</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Liste des menus existants</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>index.php/menu/******" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                    
                    <?php
                    if(!empty($displayNursery))
                    {
                    ?>
                      <th>Crèche</th>
                    <?php
                    }
                    ?>
                      <th>Date</th>
                      <th>Entrée</th>
                      <th>Plat</th>
                      <th>Dessert</th>
                      <th>Gouter</th>                      
                  </tr>
                  
                  
                    <?php
                    if(!empty($menuRecords))
                    {
                        foreach($menuRecords as $record)
                        {
                    ?>
                    <tr>
                                          
                    <?php
                    if(!empty($displayNursery))
                    {
                    ?>
                      <td><?php echo $record->nurseryName ?></td>
                    <?php
                    }
                    ?>
                      <td><?php echo $record->date ?></td>
                      <td><?php echo $record->starter ?></td>
                      <td><?php echo $record->main ?></td>
                      <td><?php echo $record->dessert ?></td>
                      <td><?php echo $record->snack ?></td>
                      <td>
                          <a href="<?php echo base_url().'index.php/menu/editOld/'.$record->id; ?>"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;</a>
                          <a href="#" data-userid="<?php echo $record->id; ?>" class="deleteMenu"><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;</a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                    
                  </table>
                  
                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "index.php/menu/menuListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>