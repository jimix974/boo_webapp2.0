<?php

$sectionId = '';
$sectionName = '';
$tablet = '';
$nurseryId = '';
$nurseryName = '';

if(!empty($sectionInfo) && count($sectionInfo)==1)
{
    foreach ($sectionInfo as $uf) // just one result
    {
        $sectionId = $uf->id;
        $sectionName = $uf->name;
        $tablet = $uf->tabletId;
        $nurseryId = $uf->nurseryId;
        $nurseryName = $uf->nurseryName;
    }
}

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
		Mise à jour d'une section
        <small> Edition d'une section</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Détails de la section</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="name">Nom</label>
                                        <input type="text" class="form-control required" id="name" name="name" maxlength="128" value="<?php echo $sectionName; ?>" >
                                    </div>
                                    
                                </div>
                                
                    <?php
                    if(!empty($displayNursery))
                    {
                    ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nursery">Crèche</label>
                                        <select class="form-control required" id="nursery" name="nursery">
                                            <option value="0">Séléctionner la crèche</option>
                                            <?php
                                            if(!empty($nurseries))
                                            {
                                                foreach ($nurseries as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>"
                                                    <?php
                                            		if($nurseryId==$rl->id)
                                            		{
                                            			echo "selected=true";
                                            		}
                                            		?>
                                                    >
                                                    	<?php echo $rl->name ?>
                                                    </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                    <?php
                    }
                    ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tablet">Tablette</label>
                                        <?php
                   						if(!empty($tablet))
                    					{
                    					?>
                                        <input type="text" disabled class="form-control required" id="tablet"  name="tablet" maxlength="128" value="<?php echo $tablet; ?>">
                                        <?php
                    					}
                    					else
                    					{
                    					?>
                    					<input type="text" disabled class="form-control required" id="tablet"  name="tablet" maxlength="128" value="Seul un administrateur peut attribuer une tablette">
                                        <?php
                    					}
                    					?>
                                    </div>
                                </div>
             
                                
                                                           
       						</div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Mettre à jour" />
                            <input type="reset" class="btn btn-default" value="Annuler" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
