<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestion des tablette
        <small>Ajouter, Modifier, Supprimer</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/tablet/addNew">Ajouter une tablette</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Liste des tablettes</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>index.php/tablet/tabletListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Identifiant unique</th>
                      <th>Clé tablette</th>
                      <th>IMEI</th>
                      <th>Crèche</th>
                      <th>Actions</th>
                    </tr>
                    <?php
                    if(!empty($tabletRecords))
                    {
                        foreach($tabletRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->idUnique ?></td>
                   	  <td><?php echo $record->tablet_key ?></td>
                      <td><?php echo $record->imei ?></td>
                      <td><?php echo $record->nurseryName ?></td>
                      <td>
                          <a href="<?php echo base_url().'index.php/tablet/editOld/'?>"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;</a>
                          <a href="#" data-userid="<?php ?>" class="deleteTablet"><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;</a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->

              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "index.php/creche/crecheListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>