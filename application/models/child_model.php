<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Child_model extends CI_Model
{

    
/**
     * for super admin
     */
    private function queryChildListingAll($searchText = '',  $page = '', $segment = '')
    {
        $this->db->select('c.id, c.name, c.firstname, c.birthday, c.sex, s.name as sectionName, c.idUnique, n.name as nurseryName');
        $this->db->from('Child as c');
        $this->db->join('Section as s', 's.id = c.Section_id','left');
        $this->db->join('Nursery as n', 'n.id = s.Nursery_id','left');
        //if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
        //$this->session->targetDB->where('BaseTbl.isDeleted', 0);
        //$this->session->targetDB->initialize();
        // manage pagination
        if(!empty($page) && !empty($segment)) {
            $this->db->limit($page, $segment);
        }
    
        $query = $this->db->get();
        return $query;
    }
    
    /**
     * for group admin
     */
    private function queryChildListingGroup($searchText = '', $groupId = '', $page = '', $segment = '')
    {
        $this->db->select('c.id, c.name, c.firstname, c.birthday, c.sex, s.name as sectionName, c.idUnique, n.name as nurseryName');
        $this->db->from('Child as c');
        $this->db->join('Section as s', 's.id = c.Section_id','left');
        $this->db->join('Nursery as n', 'n.id = s.Nursery_id','left');
        
        //TODO
        //if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
        //$this->db->where('BaseTbl.isDeleted', 0);
    
        if(!empty($groupId)) {
            $this->db->where('ng.NurseryGroup_id', $groupId);
        }

        // manage pagination
        if(!empty($page) && !empty($segment)) {
            $this->db->limit($page, $segment);
        }
        
        $query = $this->db->get();
        return $query;
    }
    
    /**
     * for nursery admin
     */
    private function queryChild($searchText = '', $nurseryId = '', $page = '', $segment = '')
    {
        $this->db->select('c.id, c.name, c.firstname, c.birthday, c.sex, s.name as sectionName, c.idUnique, n.name as nurseryName');
        $this->db->from('Child as c');
        $this->db->join('Section as s', 's.id = c.Section_id','left');
        $this->db->join('Nursery as n', 'n.id = s.Nursery_id','left');
    
        //TODO
        //if(!empty($searchText)) { $this->db->or_like('BaseTbl.email', $searchText); $this->db->or_like('BaseTbl.name', $searchText); $this->db->or_like('BaseTbl.mobile', $searchText); }
    
        if(!empty($nurseryId)) {
            $this->db->where('n.id', $nurseryId);
        }
    
        // manage pagination
        if(!empty($page) && !empty($segment)) {
            $this->db->limit($page, $segment);
        }
        
        $query = $this->db->get();
        return $query;
    }
    
    /**
     * This function is used to get the nursery listing count for super admin
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function childListingCountAll($searchText = '')
    {               
        $query = $this->queryChildListingAll($searchText);
        return count($query->result());
    }
    
    /**
     * This function is used to get the nusery listing count for super admin
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function childListingAll($searchText = '', $page = '', $segment = '')
    {        
        $query = $this->queryChildListingAll($searchText, $page, $segment);
        return $query->result();
    }
    
    /**
     * This function is used to get the nursery listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function childListingGroupCount($searchText = '', $groupId = '')
    {
        $query = $this->queryChildListingGroup($searchText, $groupId);
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the nrsery listing count for a nursery group
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function childListingGroup($searchText = '', $page = '', $segment = '', $groupId = '')
    {
        $query = $this->queryChildListingGroup($searchText, $groupId, $page, $segment);
        return $query->result();
    }

    
    /**
     * This function is used to get one nursery infos
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function childListingNursery($searchText = '', $page = '', $segment = '', $nurseryId = '')
    {
        $query = $this->queryChild($searchText, $nurseryId, $page, $segment);
        
        return $query->result();
    }



   
}


  