<script type="text/javascript">
 
    function choix(id){
 
           for( var i = 1; i <= 14; i++){
 
               if(i == id){
 
                   document.getElementById('div_'+ i).style.display = 'block';
               }
               else{
                    document.getElementById('div_'+ i).style.display ='none';
 
                }
           } 
 
    }
</script> 
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestion des enfants
        <small>Ajouter / Modifier enfant</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Entrer les informations de l'enfant</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>index.php/user/addNewUser" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Nom</label>
                                        <input type="text" class="form-control required" id="fname" name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lname">Prénom</label>
                                        <input type="text" class="form-control required" id="lname"  name="lname" maxlength="128">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="birth">Date de naissance</label>
                                        <input type="text" class="form-control required" id="date" name="date" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sexe">Sexe</label>
                                        <input type="text" class="form-control required" id="sexe"  name="sexe" maxlength="128">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="section">Section</label>
                                        <input type="text" class="form-control required" id="section" name="section" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="chilId">Identifiant unique</label>
                                        <input type="text" class="form-control required" id="chilId"  name="chilId" maxlength="128">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="parent">Parent</label>
                                        <input type="text" class="form-control required" id="parent" name="parent" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="parent2">2eme Parent</label>
                                        <input type="text" class="form-control required" id="parent2"  name="parent2" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="section">Horaire</label></br>
                                        <input  type="radio" name="type" value="Etoile" onclick="choix(1)"/> Général
                                        &nbsp;&nbsp;&nbsp;&nbsp;<input  type="radio" name="type" value="Chemin" onclick="choix(2)"/> Détaillé</br>
                                        </br>
                                                               
                                        <div id="div_1" style="display:none" >
                                            <table>
                                                <tr>
                                                    <td><label>Heure d'arrivée:&nbsp;&nbsp;</label></td>
                                                <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>

                                                    
                                                </tr>
                                                <tr>
                                                    <td><label>Heure départ:</label></td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                            </table>
                                        </div>
                                 
                                        <div id="div_2" style="display:none" >
                                            <table>
                                                <tr>
                                                    <td><label>LUNDI :</label></td>
                                                    <td>&nbsp;&nbsp;Heure d'arrivée:&nbsp;&nbsp;</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure1" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;Heure départ:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure1" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td><label>MARDI :</label></td>
                                                    <td>&nbsp;&nbsp;Heure d'arrivée:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure2" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;Heure départ:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure2" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td><label>MERCREDI :</label></td>
                                                    <td>&nbsp;&nbsp;Heure d'arrivée:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure3" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;Heure départ:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure3" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td><label>JEUDI :</label></td>
                                                    <td>&nbsp;&nbsp;Heure d'arrivée:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure4" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;Heure départ:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure4" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td><label>VENDREDI :</label></td>
                                                    <td>&nbsp;&nbsp;Heure d'arrivée:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="arrheure5" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;Heure départ:</td>
                                                    <td><div class="input-group bootstrap-timepicker timepicker">
                                                <input id="depheure5" type="text" style="margin-bottom: 5px;" class="form-control input-small"></div></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <script type="text/javascript">
                                                $('#arrheure').timepicker();
                                                $('#depheure').timepicker();
                                                $('#arrheure1').timepicker();
                                                $('#depheure1').timepicker();
                                                $('#arrheure2').timepicker();
                                                $('#depheure2').timepicker();
                                                $('#arrheure3').timepicker();
                                                $('#depheure3').timepicker();
                                                $('#arrheure4').timepicker();
                                                $('#depheure4').timepicker();
                                                $('#arrheure5').timepicker();
                                                $('#depheure5').timepicker();
                                            </script>
                                        </div>
                         

                         
                                    </div>



                                        
                                </div>
                                    
                            </div>

                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Valider" />
                            <input type="reset" class="btn btn-default" value="Reinitialiser" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>